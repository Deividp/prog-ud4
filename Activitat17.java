import java.util.Scanner;

public class Activitat17 {

    public static void main(String[] args) {

            Scanner tec = new Scanner(System.in);
            System.out.println("introduce el string;");
            String palabra = tec.nextLine();
            System.out.println(cambiaVocal(palabra));

    }

    public static String cambiaVocal (String palabra) {

        palabra = palabra.replace("e", "a");
        palabra = palabra.replace("i", "a");
        palabra = palabra.replace("o", "a");
        palabra = palabra.replace("u", "a");
        return palabra;

    }

}


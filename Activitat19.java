import java.util.Scanner;

public class Activitat19 {
    public Activitat19() {
    }

    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce texto: ");
        String palabras = tec.nextLine();
        System.out.println(esPor(palabras));
    }

    public static String esPor(String frase) {
        frase = frase.replace("es", "no por");
        System.out.println(frase);
        return frase;
    }
}

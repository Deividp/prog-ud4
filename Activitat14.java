import java.util.Scanner;

public class Activitat14 {


    public static void main(String[] args) {


        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce un numero: ");
        int num = tec.nextInt();


        System.out.println(funcionX(num));
    }

    public static int funcionX(int num){

        if(num <= 2){
            return num;
        }else{
            return funcionX(num-3)+2*funcionX(num-2)+funcionX(num-1);
        }

    }
}

import java.util.Scanner;

public class Activitat18 {


    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("introduce una frase: ");
        String frase = tec.nextLine();
        System.out.println(medioString(frase));
    }

    public static String medioString(String mitad) {
        int num = mitad.length();
        num /= 2;
        return mitad.substring(0, num);
    }

    }


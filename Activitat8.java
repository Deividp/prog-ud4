import java.util.Scanner;

public class Activitat8 {

    //
    public static void main(String[] args) {

        Scanner tec = new Scanner(System.in);

        System.out.println("Introduce cuatro numeros de un digito: ");
        System.out.println("Introduce el primer digito: ");
        int num0 = tec.nextInt();
        System.out.println("Introduce el segundo digito: ");
        int num1 = tec.nextInt();
        System.out.println("Introduce el tercero digito: ");
        int num2 = tec.nextInt();
        System.out.println("Introduce el cuarto digito: ");
        int num3 = tec.nextInt();

        capiCua(num0, num1, num2, num3);
    }

    public static void capiCua(int primero, int segundo, int tercero, int cuarto) {

        if (primero == cuarto && segundo == tercero) {

            System.out.println("Es capicua.");
        } else {
            System.out.println("No es capicua.");
        }

    }
}

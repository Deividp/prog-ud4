import java.util.Scanner;

public class Activitat4 {


    public static void main(String[] args) {

//Escriu un mètode que accepte tres arguments: un caràcter i dos enters.
// El caràcter s'ha d'imprimir.
// El primer enter indica el nombre de vegades que s'imprimirà el caràcter//
// en la línia i el segon enter indica el nombre de línies que s'han d'imprimir

        Scanner tec = new Scanner(System.in);

        System.out.println("Escrive un caracter: ");
        char letra = tec.next().charAt(0);
        System.out.println("Introduce numero de veces se repitira el caracter por linea: ");
        int veces = tec.nextInt();
        System.out.println("Introduce numero de lineas: ");
        int lineas = tec.nextInt();

       letras(letra, veces, lineas);


    }
    public static void letras (char caracter, int numVeces, int numLineas){

        for (int i = 0; i < numLineas; i++){
            System.out.println();
            for (int j = 0; j < numVeces; j++){
                System.out.print(caracter);

            }
        }
    }

}

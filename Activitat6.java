import java.util.Scanner;

public class Activitat6 {

//La media armónica de dos números es el resultado obtenido al calcular los inversos de los números,
// calcular la media y calcular el inverso del resultado. El inverso de un número es 1 / número.
// Escribe un método que acepte dos argumentos de tipo double y devuelva la media armónica de los números.
    public static void main(String[] args) {


        Scanner tec = new Scanner(System.in);
        double media;
        System.out.println("Introduce el primer numero: ");
        double num1 = tec.nextDouble();
        System.out.println("Introduce el segundo numero");
        double num2 = tec.nextDouble();

        media = 2/(1/num1 + 1/num2);

        System.out.println("La media armonica es: " + media);


    }
}

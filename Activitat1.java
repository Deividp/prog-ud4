import java.util.Scanner;

public class Activitat1 {

        public static void main(String[]args){

            Scanner tec = new Scanner(System.in);
            System.out.println("Introduce un numero: ");
            int num1Mayor = tec.nextInt();
            System.out.println("Introduce otro numero: ");
            int num2Medio = tec.nextInt();
            System.out.println("Introduce otro mas: ");
            int num3Menor = tec.nextInt();

            num1Mayor = (num1Mayor>num2Medio)?num1Mayor:num2Medio;
            num1Mayor = (num1Mayor>num3Menor)?num1Mayor:num3Menor;

            System.out.println("El numero mas alto es: " + num1Mayor);
        }
}

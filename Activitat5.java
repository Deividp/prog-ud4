import java.util.Scanner;

public class Activitat5 {

    public static void main(String[] args) {

        Scanner tec = new Scanner(System.in);

        System.out.println("Introduce una letra: ");
        char letra = tec.next().charAt(0);
        System.out.println("Introduce el numero de lineas");
        int nlineas = tec.nextInt();

        triangle(letra, nlineas);

    }

    static private void triangle(char letra , int numLinea){

        for(int j=0; j<3; j++) {
            for(int i=0; i<3; i++) {
                System.out.print( letra );
            }
            System.out.println();
        }
    }
    }

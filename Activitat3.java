import java.awt.image.VolatileImage;
import java.util.Scanner;

public class Activitat3 {

    //Activitat3.- Escriu un mètode que accepte dos arguments:
    // el caràcter que es desitja imprimir i el nombre de vegades que s'ha d'imprimir.

    public static void main(String[]args){

        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce un caracter: ");
        char letra = tec.next().charAt(0);
        System.out.println("Introduce un numero: ");
        int num = tec.nextInt();
        System.out.println();
        for (int i = 0; i < num; i++){
            System.out.print(letra);
        }

    }
}
